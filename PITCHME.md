<!-- theme: uncover -->

#### Histoires d'un SRE perfectionniste sous pression

_Meetup AFPy Lyon - 29 septembre 2022_

![logo AFPy Lyon](img/200-afpylyon.png)  ![OVHcloud logo](img/200-ovhcloud-rect.png)


---

# Témoignages

---

# 🧑‍🦰

**Analyser des usages 📈**

> Les logs des applications me permettent de savoir comment les internautes visitent les sites que je gère.

_- Camille, service commercial_

---

# 🧑🏿

**Construire du code 🛠️**

> Je ne développe pas d’application sans gestion des logs. C’est plus clair et facile: prod ou dev, affichage en console et/ou écriture en fichier.

_- Ali, service informatique_

---

# 🧑🏼‍🦲

**Respecter la loi ⚖️**

> Je dois pouvoir retrouver a tout moment qui a accédé à quelle ressource avec quelles meta-données. Des logs bien archivé sont indispensable pour ne pas finir en prison

_- Alex, service administratif_

---

# 👨🏾‍🦱

**Surveiller des services 👀**

> Pour mieux gérer notre infrastructure, des logs de qualité c’est la base. S’assurer de la dispo des services, accéder aux logs pendant un incident ou encore corréler des situation sur un parc entier.

_- Nat, adminsys_

---

# 🦕

**Non mais franchement!**

> Pour les logs, faut se connecter aux machines. Du `grep`, un peu de `awk` et des boucles avec `ssh` si tu as plusieurs machines.

_- Didi, dinosaure_

---

# 🗣️

**Parler avec des humains**

> Hopla 👋
> Construire & utiliser des outils 🛠️
> Soigneur de pool ZFS chez OVH 🐔
> 🐍	 	 	🗻 	 	 	 	👪

_- fred z, `VU.ops` / `PU.storage`_

---

**🔊 De quoi va-t on parler 🔊**

* De bonnes pratiques 🐍
* Une `CLI` qui cherche dans [Logs Data Platform](https://www.ovhcloud.com/fr/logs-data-platform/) 👀
* Documenter / publier 📝

---

**🔇**

* D’écrire dans un flux LDP
* Du litige _Elastic_ / _AWS_
* Du nommage du projet _OpenSearch_

---

**⛔**

_Si je…_

* commence à troller 🧌
* prononce des noms: _musk_, _bezos_, _gates_, … ✊
* parle du _manager OVH_ 🐌


---

# 🤔 S.R.E. ?

* > Site Reliability Engineer

---

**🤔 S.R.E. ?**

> It’s what happens when a software engineer is tasked with what used to be called operations.

[- _Ben Treynor_, SRTsar @ Gxxxxx](https://techcrunch.com/2016/03/02/are-site-reliability-engineers-the-next-data-scientists/)

---

# 💡 Logs Data Platform

[![OpenSearch logo](img/96-opensearch.png)](https://opensearch.org/) [![LDP logo](img/200-ovh-ldp.png)](https://docs.ovh.com/fr/logs-data-platform/) [![Python logo](img/python.png)](https://python.org)

---

![ldp scheme](img/ovh-ldp-scheme.png)

---

**💡 Logs Data Platform**

* **Analyse**(Graylog, Kibana ou Grafana)
* **Stockage** (chaud / froid)
* **Collecte** (Syslog-ng, Fluentd, NXLog, Flowgger et Logstash)
    - [installation](https://docs.ovh.com/fr/logs-data-platform/how-to-log-your-linux/)
    - [«mon» rôle `ansible`](https://gitlab.com/forga/tool/ansible/debian/-/blob/stable/tasks/system_cfg/ldp.yml)

---

**💡 Contexte**

* Pas mon équipe 🤝
* De _zéro_ à `hello` 🚀
* Communiquer 📢
* Partager ♻️

---

# 🤝 Collaborer

---

# 🤝 Collaborer commence avec ![git logo](img/96-git.png) `git`

---

**🤝 Collaborer** (ensuite)

---

**🧑‍🤝‍🧑 Entre humains 📢**

* recherche info (docs!!)
* modif de service existant
* retour d’info (docs!!)

---

**⚖️ [`pyproject.toml`](https://gitlab.com/forga/tool/ovh/ldpy/-/blob/stable/pyproject.toml)**

* [**PEP 518**](https://peps.python.org/pep-0518/) – `Minimum Build System Requirements for Python Projects`
* [**PEP 621**](https://peps.python.org/pep-0621/) – `Storing project metadata in pyproject.toml`
* [**PEP 660**](https://peps.python.org/pep-0660/) – `Editable installs for pyproject.toml based builds`
* [🔥 _L’enfer du packaging Python_ 🔥](http://free_zed.gitlab.io/articles/2022/05/lenfer-du-packaging-python/) - _Guillaume Ayoub_

---

**🔧 [`Makefile`](https://gitlab.com/forga/tool/ovh/ldpy/-/blob/stable/Makefile)**

```bash
user@laptop ~/git/ldpy % make help
help       Print help on Makefile
pre_commit Run the pre-commit hook
clean      Remove files not tracked in source control
lint       Lint code
test       Test code
test_pdb   Test code and open debugger if fail
```

---

**🪝 [`git hook`](https://gitlab.com/forga/tool/ovh/ldpy#-devel-quick-start)**

```bash
user@laptop ~/git/ldpy % make pre_commit
✅ black
✅ pflake8
✅ pydocstyle
✅ pylint
```

---

**🪝 [`git hook`](https://gitlab.com/forga/tool/ovh/ldpy#-devel-quick-start)**

```bash
make pre_commit
✅ black
✅ pflake8
./tests_ldpy.py:84 in public function `test_strip_demo_entries`:
        D202: No blank lines allowed after function docstring (found 1)
🚨 pydocstyle
************* Module ldpy
ldpy.py:96:23: W0613: Unused argument 'source' (unused-argument)
************* Module tests_ldpy
tests_ldpy.py:105:4: E1111: Assigning result of a function call, where the function has no return (assignment-from-no-return)
🚨 pylint
```

---

**`Talk is cheap. Show me the code.`**

* 🔗 [`gitlab.com/forga/tool/ovh/ldpy`](https://gitlab.com/forga/tool/ovh/ldpy)
* 💡 [Un script python durable](https://vincent.bernat.ch/fr/blog/2019-script-python-durable), par _Vincent Bernat_

---

[`opensearch-py`](https://opensearch.org/docs/latest/clients/python/) [💾](https://gitlab.com/forga/tool/ovh/ldpy/-/issues/1)

```bash
pip install opensearch-py
```

```python
from opensearchpy import OpenSearch

opnsrch_clt = OpenSearch(
    LDP_CLUSTER,
    http_auth=(LDP_CRED_VALUE, LDP_CRED_TYPE),
    scheme="https",
    port=9200,
)
```

---

**Paramétrable**

[`argparse`](https://docs.python.org/3/library/argparse.html) [💾](https://gitlab.com/forga/tool/ovh/ldpy/-/issues/1)

```
❯ ./ldpy.py -h
usage: ldpy.py [-h] [-d] [-m | -l [INT]]

A basic opensearch.org client.
All start here.

optional arguments:
  -h, --help                show this help message and exit
  -m, --mapping             Show the stream mapping (default: False)
  -l [INT], --last [INT]    Last entries of the stream (default: None)
log setting:
  -d, --debug           Log activity in console (default: False)
```

---

[`logging`](https://docs.python.org/3/library/logging.html) [💾](https://gitlab.com/forga/tool/ovh/ldpy/-/issues/2)

```python
>>> import logging
>>> logging.warning('Watch out!')
WARNING:root:Watch out!
```

* `stdout`, système, fichier, etc.

---

**Documenté et fiable**

* [`pydocstyle`](http://www.pydocstyle.org/)
* [`pytest`](https://docs.pytest.org/)
* [tuto - Kevin Ndung'u Gathuku](https://semaphoreci.com/community/tutorials/testing-python-applications-with-pytest)
* [Pytest, ou comment gagner 10 ans d'espérance de vie](https://www.meetup.com/fr-FR/python-afpy-lyon/events/pvlzcpyxdblc/)

---

**Bonus**

[📼 `vcrpy`](https://vcrpy.readthedocs.io/) [💾](https://gitlab.com/forga/tool/ovh/ldpy/-/issues/4)

> VCR.py simplifies and speeds up tests that make HTTP requests.

---

```
(ldpy) 0:34 user@laptop ~/git/ldpy % make test
~/.venvs/ldpy/bin/pytest tests_*.py
=========================== test session starts ===========================
tests_ldpy.py .............................................................[100%]

=========================== 61 passed in 19.19s ===========================
```


```
(ldpy) 0:34 user@laptop ~/git/ldpy % make test
~/.venvs/ldpy/bin/pytest tests_*.py
=========================== test session starts ===========================
tests_ldpy.py .............................................................[100%]

=========================== 61 passed in 2.76s ============================
(ldpy) 0:34 user@laptop ~/git/ldpy %
```
---

# 🧑‍🏫

> J'ai vu qu'avoir des logs centralisés et disponible facilement était une réalité. Si je suis seul ou en petite équipes la question _elle est vite répondu_, LDP se relie a mon système et gère le reste.

_- Toi qui vient d'assister à cette présentation_

---

# 🔗 Liens

[`discuss.afpy.org`](https://discuss.afpy.org/invites/yMf2gEy2iY)
[Logs Data Platform](https://www.ovhcloud.com/fr/logs-data-platform/)
[`aiven/demo-opensearch-python`](https://github.com/aiven/demo-opensearch-python)
[`opensearch` docs](https://opensearch.org/docs/latest/) ([`query-dsl`](https://opensearch.org/docs/latest/opensearch/query-dsl/index/), [`search-template`](https://opensearch.org/docs/latest/opensearch/search-template/))

---

**⁉️ Questions , remarques, réclamations, etc.**

![QRcode](img/qrcode-fred.zind.fr.png) http://fred.zind.fr
